

//load data table
$(document).ready(function() {
    $("#btn-seach").click(function() {
        getForecast()
    })
})

    // Mẫu gọi API lấy dữ liệu dự báo thời tiết
    function getForecast() {
        var vObj = {
            city : "" , 
            days:""
        }
        getDataInp(vObj)
        var vCkeck = validateDataInt(vObj);
        if(vCkeck == true){
            if (vObj.city != '' && vObj.days != '') {
                $.ajax({
                    url: 'http://api.openweathermap.org/data/2.5/forecast/daily?q=' + vObj.city + "&units=metric" +
                        "&cnt=" + vObj.days + "&APPID=c10bb3bd22f90d636baa008b1529ee25",
                    type: "GET",
                    dataType: "json",
                    success: function (data) {
    
                        for (var i = 0; i < data.list.length; i++) {
                            $("<tr>").html(
                                `<td>${i+1}</td>
                                <td> <img src="http://openweathermap.org/img/w/${data.list[i].weather[0].icon}.png"></td>
                                <td>${ data.list[i].weather[0].main}</td>
                                <td>${data.list[i].weather[0].description}</td>
                                <td>${data.list[i].temp.morn}${"℃"}</td>
                                <td>${data.list[i].temp.night}${"℃"}</td>
                                <td>${ data.list[i].temp.min}${"℃"}</td>
                                <td>${data.list[i].temp.max}${"℃"}</td>
                                <td>${data.list[i].pressure}${"hpa"}</td>
                                <td>${data.list[i].humidity}${"%"}</td>
                                <td>${data.list[i].speed}${"m/s"}</td>
                                <td>${data.list[i].deg}${"°"}</td>
                                `
                            ).appendTo($("#table-body"))
                            
                        }
    
                        
                    }
                });
            } else {
                console.error("Input not valid")
            }
        }
       
    };
    // validate data int
    function validateDataInt(paramOjb) {
        if(paramOjb.city ==""){
            alert("nhập thành phố")
            return false
        }
        if(paramOjb.days ==""){
            alert("nhập ngày")
            return false
        }
        return true
    }
    //get data int
    function getDataInp(paramOjb) {
        paramOjb.city = $("#inp-city").val();
        paramOjb.days = $("#inp-day").val();

    }
   